﻿using System.Threading.Tasks;

namespace Hjklvfr.Brute.Checker.Abstractions
{
    public interface ILoginPasswordChecker
    {
        Task<bool> CheckLoginPasswordAsync(string login, string password);
    }
}