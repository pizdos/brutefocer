﻿using System;
using System.Net;

namespace Hjklvfr.Brute.Checker.NBIKemSU.Exceptions
{
    public class NotMovedPermanentlyResponseCodeException : Exception
    {
        public readonly HttpStatusCode StatusCode;

        public NotMovedPermanentlyResponseCodeException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }
    }
}