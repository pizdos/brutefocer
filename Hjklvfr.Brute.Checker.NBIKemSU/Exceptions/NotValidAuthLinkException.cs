﻿using System;

namespace Hjklvfr.Brute.Checker.NBIKemSU.Exceptions
{
    public class NotValidAuthLinkException : Exception
    {
        public string AuthLink;

        public NotValidAuthLinkException(string authLink)
        {
            AuthLink = authLink;
        }
    }
}