﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Hjklvfr.Brute.Checker.NBIKemSU.Extensions
{
    public static class Cookies
    {
        public static IEnumerable<Cookie> ExtractCookies(this HttpResponseMessage response)
        {
            if (!response.Headers.TryGetValues("Set-Cookie", out var cookieEntries))
            {
                return Enumerable.Empty<Cookie>();
            }

            var uri = response.RequestMessage.RequestUri;
            var cookieContainer = new CookieContainer();

            foreach (var cookieEntry in cookieEntries)
            {
                cookieContainer.SetCookies(uri, cookieEntry);
            }

            return cookieContainer.GetCookies(uri).Cast<Cookie>();
        }

        public static void PopulateCookies(this HttpRequestMessage request, IEnumerable<Cookie> cookies)
        {
            request.Headers.Remove("Cookie");
            var cookiesList = cookies.ToArray(); // array because use foreach
            if (cookiesList.Any())
            {
                request.Headers.Add("Cookie", cookiesList.ToHeaderFormat());
            }
        }

        private static string ToHeaderFormat(this IEnumerable<Cookie> cookies)
        {
            var cookieString = string.Empty;
            var delimiter = string.Empty;

            foreach (var cookie in cookies)
            {
                cookieString += delimiter + cookie;
                delimiter = "; ";
            }

            return cookieString;
        }
    }
}