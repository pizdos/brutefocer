﻿using System.Net.Http;
using Hjklvfr.Brute.Checker.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Brute.Checker.NBIKemSU.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNbiKemSuChecker(this IServiceCollection services)
        {
            services.AddHttpClient(NbiKemSuClient.ClientName)
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
                {
                    AllowAutoRedirect = false,
                    UseCookies = false // todo: add configured proxy pool
                });

            services.AddTransient<INbiKemSuClient>(ctx =>
            {
                var clientFactory = ctx.GetRequiredService<IHttpClientFactory>();
                var httpClient = clientFactory.CreateClient(NbiKemSuClient.ClientName);

                return new NbiKemSuClient(httpClient);
            });

            services.AddTransient<ILoginPasswordChecker, NbiKemSuChecker>();

            return services;
        }
    }
}