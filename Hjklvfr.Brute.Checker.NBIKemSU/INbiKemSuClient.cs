﻿using System.Threading.Tasks;

namespace Hjklvfr.Brute.Checker.NBIKemSU
{
    public interface INbiKemSuClient
    {
        Task<bool> TryLogin(string login, string password);
    }
}