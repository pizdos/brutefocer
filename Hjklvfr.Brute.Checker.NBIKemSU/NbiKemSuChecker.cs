﻿using System.Threading.Tasks;
using Hjklvfr.Brute.Checker.Abstractions;

namespace Hjklvfr.Brute.Checker.NBIKemSU
{
    public class NbiKemSuChecker : ILoginPasswordChecker
    {
        private readonly INbiKemSuClient _client;

        public NbiKemSuChecker(INbiKemSuClient client)
        {
            _client = client;
        }

        public async Task<bool> CheckLoginPasswordAsync(string login, string password)
        {
            return await _client.TryLogin(login, password);
        }
    }
}