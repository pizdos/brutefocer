﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Hjklvfr.Brute.Checker.NBIKemSU.Exceptions;
using Hjklvfr.Brute.Checker.NBIKemSU.Extensions;

namespace Hjklvfr.Brute.Checker.NBIKemSU
{
    public class NbiKemSuClient : INbiKemSuClient
    {
        public static string ClientName => "NBIKemSU";

        private readonly HttpClient _client;

        private IEnumerable<Cookie> _cookies;

        public NbiKemSuClient(HttpClient client)
        {
            _client = client;
        }

        private async Task<Uri> GetAuthUri()
        {
            var responsePage = await _client.GetAsync("https://moodle.nbikemsu.ru/auth/oidc/");

            var responsePageStatusCode = responsePage.StatusCode;
            if (responsePageStatusCode != HttpStatusCode.SeeOther)
                throw new NotMovedPermanentlyResponseCodeException(responsePageStatusCode);

            if (!responsePage.Headers.TryGetValues("Location", out var headerValues))
                throw new HaveNotLocationHeaderValueException();

            var authLink = headerValues.First();

            _cookies = responsePage.ExtractCookies(); // todo: optimize (use when all is valid)

            if (Uri.TryCreate(authLink, UriKind.Absolute, out var authUri))
            {
                if (authUri.Scheme == Uri.UriSchemeHttps)
                {
                    return authUri;
                }
            }
            else
                throw new NotValidAuthLinkException(authLink);

            throw new Exception();
        }

        public async Task<bool> TryLogin(string login, string password)
        {
            var authUri = await GetAuthUri();
            var loginForm = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                {"UserName", login},
                {"Password", password},
                {"AuthMethod", "FormsAuthentication"}
            });

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, authUri)
            {
                Content = loginForm
            };
            requestMessage.PopulateCookies(_cookies);

            var resultResponse = await _client.SendAsync(requestMessage);
            return resultResponse.StatusCode == HttpStatusCode.Found;
        }
    }
}