﻿using System.Threading.Tasks;

namespace Hjklvfr.Brute.Processor.Abstractions
{
    public interface IBruteProcessor
    {
        Task ProcessAsync();
    }
}