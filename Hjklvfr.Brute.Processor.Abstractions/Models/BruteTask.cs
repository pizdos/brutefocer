﻿using System;
using System.Collections.Generic;

namespace Hjklvfr.Brute.Processor.Abstractions.Models
{
    public class BruteTask
    {
        public readonly string Login;
        
        public readonly IEnumerable<string> Passwords;

        public readonly int ThreadCount;
        
        public readonly Action<string> OnBad;
        
        public readonly Action<string> OnGood;

        public BruteTask(string login,
            IEnumerable<string> passwords,
            Action<string> onGood = default, 
            Action<string> onBad = default,
            int threadCount = 1)
        {
            Login = login ?? throw new ArgumentNullException(nameof(login));
            Passwords = passwords ?? throw new ArgumentNullException(nameof(passwords));
            OnGood = onGood;
            ThreadCount = threadCount;
            OnBad = onBad;
        }
    }
}
