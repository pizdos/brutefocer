﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Hjklvfr.Brute.Processor.Dataflow.Messages;

namespace Hjklvfr.Brute.Processor.Dataflow.Blocks
{
    public class After
    {
        public readonly ActionBlock<CheckReport> Block;

        private readonly CancellationTokenSource _cancellationTokenSource;

        private readonly Action<string> _onGood;
        
        private readonly Action<string> _onBad;

        public After(CancellationTokenSource cancellationTokenSource, Action<string> onGood, Action<string> onBad)
        {
            _cancellationTokenSource = cancellationTokenSource;
            _onGood = onGood;
            _onBad = onBad;
            Block = new ActionBlock<CheckReport>(
                Process,
                new ExecutionDataflowBlockOptions
                {
                    TaskScheduler = TaskScheduler.FromCurrentSynchronizationContext()
                }
            );
        }

        private void Process(CheckReport message)
        {
            if (message.IsSuitable)
            {
                _cancellationTokenSource.Cancel();
                _onGood(message.Password);
            }
            else
                _onBad?.Invoke(message.Password);
        }
    }
}