﻿using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Hjklvfr.Brute.Checker.Abstractions;
using Hjklvfr.Brute.Processor.Dataflow.Messages;

namespace Hjklvfr.Brute.Processor.Dataflow.Blocks
{
    public class Check
    {
        public readonly TransformBlock<string, CheckReport> Block;

        private readonly string _login;

        private readonly ILoginPasswordChecker _checker;

        public Check(ILoginPasswordChecker checker, string login, ExecutionDataflowBlockOptions options)
        {
            _checker = checker;
            _login = login;
            Block = new TransformBlock<string, CheckReport>(
                async pass =>
                    await CheckAsync(pass)
                        .ConfigureAwait(false),
                options
            );
        }

        public void LinkTo(After after)
        {
            Block.LinkTo(after.Block, new DataflowLinkOptions
            {
                PropagateCompletion = true
            });
        }

        private async Task<CheckReport> CheckAsync(string password)
        {
            var checkResult = await _checker.CheckLoginPasswordAsync(_login, password)
                .ConfigureAwait(false);

            return new CheckReport(password, checkResult);
        }
    }
}