﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Hjklvfr.Brute.Checker.Abstractions;
using Hjklvfr.Brute.Processor.Abstractions;
using Hjklvfr.Brute.Processor.Abstractions.Models;
using Hjklvfr.Brute.Processor.Dataflow.Blocks;

namespace Hjklvfr.Brute.Processor.Dataflow
{
    public class DataflowBruteProcessor : IBruteProcessor
    {
        private readonly IEnumerable<string> _passwords;

        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        private readonly After _after;

        private readonly Check _check;

        public DataflowBruteProcessor(
            BruteTask bruteTask,
            ILoginPasswordChecker checkerService)
        {
            _passwords = bruteTask.Passwords;

            _check = new Check(checkerService, bruteTask.Login,
                new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = bruteTask.ThreadCount,
                    CancellationToken = _cancellationTokenSource.Token
                });
            _after = new After(_cancellationTokenSource, bruteTask.OnGood, bruteTask.OnBad);

            _check.LinkTo(_after);
        }

        public async Task ProcessAsync()
        {
            foreach (var password in _passwords)
            {
                if (_cancellationTokenSource.IsCancellationRequested) break;
                await _check.Block.SendAsync(password).ConfigureAwait(false);
            }

            _check.Block.Complete();

            await _after.Block.Completion.ConfigureAwait(false);
        }
    }
}