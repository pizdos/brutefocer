﻿namespace Hjklvfr.Brute.Processor.Dataflow.Messages
{
    public class CheckReport
    {
        public CheckReport(string password, bool isVerified)
        {
            Password = password;
            IsSuitable = isVerified;
        }

        public readonly bool IsSuitable;
        
        public readonly string Password;
    }
}