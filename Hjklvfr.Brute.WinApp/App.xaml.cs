﻿using System;
using System.Windows;
using Hjklvfr.Brute.Checker.NBIKemSU.Extensions;
using Hjklvfr.Brute.WinApp.Views;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Brute.WinApp
{
    public partial class App
    {
        private IServiceProvider ServiceProvider { get; set; }

        private AppPresenter _presenter;

        protected override void OnStartup(StartupEventArgs e)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            ServiceProvider = services.BuildServiceProvider();

            var mainWindow = (MainWindow) ServiceProvider.GetRequiredService<IAppView>();
            _presenter = ServiceProvider.GetRequiredService<AppPresenter>();
            _presenter.BruteFinished += mainWindow.presenter_BruteFinished;
            mainWindow.Show();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddNbiKemSuChecker();
            services.AddSingleton(typeof(AppPresenter));
            services.AddSingleton<IAppView, MainWindow>();
        }
    }
}