﻿using System;
using System.Windows;
using Hjklvfr.Brute.Checker.Abstractions;
using Hjklvfr.Brute.Processor.Abstractions.Models;
using Hjklvfr.Brute.Processor.Dataflow;
using Hjklvfr.Brute.WinApp.Events;
using Hjklvfr.Brute.WinApp.Views;

namespace Hjklvfr.Brute.WinApp
{
    public class AppPresenter
    {
        private readonly IAppView _view;
        private readonly ILoginPasswordChecker _checker;

        public event EventHandler BruteFinished = delegate { };

        public AppPresenter(IAppView appView, ILoginPasswordChecker checker)
        {
            _view = appView;
            _view.BruteStarted += view_BruteforceStartedAsync;

            _checker = checker;
        }

        private async void view_BruteforceStartedAsync(object sender, BruteforceEventArgs e)
        {
            var bruteTask = new BruteTask(
                e.Login,
                e.Passwords,
                password => _view.FoundPass(password),
                password => _view.LogPass(password, false),
                e.ThreadCount);

            await new DataflowBruteProcessor(bruteTask, _checker).ProcessAsync().ConfigureAwait(false);

            Application.Current.Dispatcher.Invoke(() =>
                BruteFinished?.Invoke(this, EventArgs.Empty));
        }
    }
}