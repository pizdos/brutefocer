﻿using System;
using System.Collections.Generic;

namespace Hjklvfr.Brute.WinApp.Events
{
    public class BruteforceEventArgs : EventArgs
    {
        public string Login { get; }

        public List<string> Passwords { get; }

        public int ThreadCount { get; }

        public BruteforceEventArgs(string login, List<string> passwords, int threadCount)
        {
            Login = login;
            Passwords = passwords;
            ThreadCount = threadCount;
        }
    }
}