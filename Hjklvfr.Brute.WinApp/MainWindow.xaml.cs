﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Hjklvfr.Brute.WinApp.Events;
using Hjklvfr.Brute.WinApp.Views;
using Microsoft.Win32;

namespace Hjklvfr.Brute.WinApp
{
    public partial class MainWindow : IAppView
    {
        private string _fileName;

        public MainWindow()
        {
            InitializeComponent();
        }

        public IEnumerable<string> Passwords { get; private set; }

        public void FoundPass(string password)
        {
            passwordLabel.Content = password;
        }

        public void NotFoundPass()
        {
            passwordLabel.Content = "Not Found";
        }

        public void LogPass(string pass, bool success)
        {
            Logs.Children.Add(new Label
            {
                Content = pass
            });
        }

        public event EventHandler<BruteforceEventArgs> BruteStarted = delegate { };

        public void presenter_BruteFinished(object sender, EventArgs eventArgs)
        {
            UiStartUpdate();
        }

        private void UiStartUpdate()
        {
            StartButton.Content = "Start";
            StartButton.IsEnabled = true;
        }

        private void UiStopUpdate()
        {
            StartButton.IsEnabled = false;
            StartButton.Content = "Brute...";
            passwordLabel.Content = "No Password";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Passwords = File.ReadLines(_fileName)
                .Distinct()
                .Where(l => !l.Equals(string.Empty));

            Logs.Children.Clear();

            UiStopUpdate();
            BruteStarted?.Invoke(this,
                new BruteforceEventArgs(Login.Text, Passwords.ToList(), int.Parse(ThreadCount.Text)));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void FileButton_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Multiselect = false,
                Filter = "Text files (*.txt)|*.txt",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            };
            if (openFileDialog.ShowDialog() != true) return;
            _fileName = openFileDialog.FileName;
            FilePath.Text = _fileName;

            StartButton.IsEnabled = IsReady();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                DragMove();
            }
        }

        private bool IsReady()
        {
            return ThreadCount.Text.Length != 0 &&
                   Login.Text.Length > 3 &&
                   !string.IsNullOrEmpty(_fileName);
        }

        private void Login_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox) sender;
            if (textBox.Text.Equals("Username"))
                textBox.Text = string.Empty;
        }

        private void Login_LostFocus(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox) sender;
            if (textBox.Text.Equals(string.Empty))
                textBox.Text = "Username";
        }

        private void Login_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!IsLoaded) return;
            StartButton.IsEnabled = IsReady();
        }

        private void ThreadCount_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox) sender;
            if (textBox.Text.Equals("Thread Count"))
                textBox.Text = string.Empty;
        }

        private void ThreadCount_LostFocus(object sender, RoutedEventArgs e)
        {
            var textBox = (TextBox) sender;
            if (textBox.Text.Equals(string.Empty))
                textBox.Text = "Thread Count";
        }

        private void ThreadCount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ThreadCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!IsLoaded) return;
            StartButton.IsEnabled = IsReady();
        }
    }
}