﻿using System;
using System.Collections.Generic;
using Hjklvfr.Brute.WinApp.Events;

namespace Hjklvfr.Brute.WinApp.Views
{
    public interface IAppView
    {
        IEnumerable<string> Passwords { get; }

        void FoundPass(string password);
        void NotFoundPass();

        void LogPass(string pass, bool success);

        event EventHandler<BruteforceEventArgs> BruteStarted;
    }
}